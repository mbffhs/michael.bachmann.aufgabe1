package ch.ffhs.students.vlcwy;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class Main extends Application {

    static final Logger logger = LogManager.getLogger(Main.class);

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Hello World!");
        Button btn = new Button();
        Label lbl = new Label("n.A.");
        lbl.setId("label");
        btn.setText("Click me!");
        btn.setId("clickMeButton");
        btn.setOnAction(event -> {
            logger.trace("Button was pressed123");
            lbl.setText("Hello World!");
        });

        HBox root = new HBox();
        root.setSpacing(10);
        root.getChildren().addAll(btn, lbl);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }

    public void notCoveredMethod() {
        String temp = "This is a test";
        logger.trace(temp);
    }
}
