package ch.ffhs.students.vlcwy;

import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.util.DebugUtils;

import java.util.function.Function;

import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.util.NodeQueryUtils.hasText;

@ExtendWith(ApplicationExtension.class)
public class ClickApplicationTest {

    private static Main myApp;

    @BeforeAll
    public static void setup() throws Exception {
        FxToolkit.registerPrimaryStage();
        myApp = (Main)FxToolkit.setupApplication(Main.class);
    }

    @AfterAll
    public static void cleanup() {}

    @Test
    public void test_one(FxRobot robot) {

        myApp.notCoveredMethod();

        verifyThat("#clickMeButton", hasText("Click me!"));
        verifyThat("#label", hasText("n.A."));

        // click on the button
        robot.clickOn("#clickMeButton");

        verifyThat("#label", hasText("Hello World!"));
    }
}
